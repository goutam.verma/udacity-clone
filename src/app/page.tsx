import Navbar from "@/components/Navbar/Navbar";
import Auth from "@/components/Auth/Auth";

export default function Home() {
  return (
      <main className="h-screen flex flex-col py-20 items-center bg-gray-100 mt-10 mb-10">
          <Navbar />
          <Auth />
      </main>
  );
}
