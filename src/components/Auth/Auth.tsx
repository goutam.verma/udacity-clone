"use client";
import React, { useState } from "react";
import SignInForm from "@/components/Auth/SignIn";
import SignUpForm from "@/components/Auth/SignUp";
import clsx from 'clsx';

enum Mode {
  SignIn,
  SignUp,
}

const Auth: React.FC = () => {
  const [mode, setMode] = useState(Mode.SignUp);

  const toggleMode = (selectedMode: Mode) => {
    setMode(selectedMode);
  };
  return (
    <div className="flex flex-col items-center">
      <div className="flex justify-around w-full">
        <button
          className={clsx("w-full text-sm font-sans p-4 h-full text-center font-semibold cursor-pointer", {
            "bg-white": mode === Mode.SignUp,
          })}
          onClick={() => toggleMode(Mode.SignUp)}
        >
          SIGN UP
        </button>
        <button
          className={clsx("w-full text-sm font-sans p-4 h-full text-center font-semibold cursor-pointer", {
            "bg-white": mode === Mode.SignIn,
          })}
          onClick={() => toggleMode(Mode.SignIn)}
        >
          SIGN IN
        </button>
      </div>

      <div className="w-full">
        {mode === Mode.SignIn && <SignInForm />}
        {mode === Mode.SignUp && <SignUpForm />}
      </div>
    </div>
  );
};

export default Auth;