"use client";
import React, {useState} from "react";
import Loader from "@/components/Loader/Loader";
import Image from "next/image";
import clsx from "clsx";

interface SignInFields {
  email: string;
  password: string;
}

const SignInForm = () => {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [errors, setErrors] = useState<SignInFields>({
    email: "",
    password: "",
  });
  const [touched, setTouched] = useState<{
    [key in keyof SignInFields]: boolean;
  }>({ email: false, password: false });
  const [isFormValid, setIsFormValid] = useState<boolean>(false);
  const [isSignInClicked, setIsSignInClicked] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [showPassword, setShowPassword] = useState<boolean>(false);

  const validateForm = () => {
    let emailError = "";
    let passwordError = "";

    if (email === "") {
      emailError = "Email is required";
    } else if (!email.includes("@")) {
      emailError = "Invalid email format";
    }

    if (password === "") {
      passwordError = "Password is required";
    } else if (password.length < 8) {
      passwordError = "Password must be at least 8 characters long";
    }

    setErrors({ email: emailError, password: passwordError });
    setIsFormValid(!emailError && !passwordError);
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    validateForm();
    const { name, value } = event.target;
    if (name === "email") {
      setTouched({ ...touched, email: true });
      setEmail(value);
    } else if (name === "Password") {
      setTouched({ ...touched, password: true });
      setPassword(value);
    }
  };

  const handleSubmit = async () => {
    validateForm();
    setIsSignInClicked(true);
    setIsLoading(true);

    if (isFormValid) {
      try {
        const response = await fetch("https://reqres.in/api/login", {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            email,
            password,
          }),
        });

        if (!response.ok) {
          const errorData = await response.json();
          alert(errorData.error);
        }

        const data = await response.json();

        if (data.token) {
          alert("Login successful");
        } else if (data.error) {
          alert(data.error);
        } else {
          alert("Failed to login");
        }
      } catch (error) {
        console.error("Error:", error);
      } finally {
        setIsLoading(false);
      }
    } else {
      setIsLoading(false);
    }
  };

  return (
    <div className="relative">
      {isLoading && <Loader />}

      <div className="p-10 bg-white shadow-md rounded-md">
        <div className="mb-2">
          <h2 className="text-4xl text-center font-medium text-black font-polySans leading-15">
            Sign in to your account.
          </h2>
          <p className="text-black text-center py-4">
            Build skills for today, tomorrow, and beyond. <br></br>Education to
            future-proof your career.
          </p>
        </div>
        {isSignInClicked && !isFormValid && (
          <div className="text-start p-5 font-polySans items-center bg-orange-600 rounded">
            <p className="text-white text-base">
              Must specify an email and password
            </p>
          </div>
        )}

        <div className="flex flex-col sm:flex-row justify-around items-center mt-4">
          <button className="bg-white p-2 hover:bg-gray-100 text-black py-2 px-4 border border-gray-400 rounded flex items-center mb-2 sm:mb-0 sm:w-auto w-full justify-center">
            <Image
              src="/google-icon.svg"
              alt="Google Icon"
              className="h-5 w-5 mr-2"
              width={5}
              height={5}
            />
            <span className="text-sm">Sign in with Google</span>
          </button>

          <button className="bg-white p-2 hover:bg-gray-100 text-black py-2 px-2 border border-gray-400 rounded flex items-center sm:w-auto w-full justify-center">
            <Image
              src="/facebook-icon.svg"
              alt="Facebook Icon"
              className="h-5 w-5 mr-2"
              width={5}
              height={5}
            />
            <span className="text-sm">Sign in with Facebook</span>
          </button>
        </div>

        <div className="mb-4">
          <div className="mb-6 h-5 border-b border-gray-400 py-2 px-2 p-2 text-center">
            <label className="bg-white px-5 font-medium">or</label>
          </div>

          <input
            type="email"
            id="email"
            name="email"
            className={clsx(
              "mb-3 w-full p-2 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-700 focus:border-2",
              touched.email && errors.email && "border-red-500"
            )}
            placeholder="Email address"
            onChange={handleInputChange}
          />
          {touched.email && errors.email && (
            <p className="text-red-500 text-sm">{errors.email}</p>
          )}
          <div className="relative">
            <input
              type={showPassword ? "text" : "password"}
              id="password"
              name="Password"
              className={clsx(
                "w-full p-2 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-700 focus:border-2",
                touched.password && errors.password && "border-red-500"
              )}
              placeholder="Password"
              onChange={handleInputChange}
            />
            <button
              type="button"
              aria-label={
                showPassword ? "Password Visible" : "Password Invisible."
              }
              className="text-black dark:text-white absolute"
              onClick={() => {
                setShowPassword((prev) => !prev);
              }}
              style={{
                right: "10px",
                top: "50%",
                transform: "translateY(-50%)",
              }}
            >
              {showPassword ? (
                <Image
                  src="/invisible-password-icon.svg"
                  alt="Google Icon"
                  width={24}
                  height={24}
                />
              ) : (
                <Image
                  src="/visible-password-icon.svg"
                  alt="Invisible Password"
                  width={24}
                  height={24}
                />
              )}
            </button>
          </div>
          {touched.password && errors.password && (
            <p className="text-red-500 py-1 text-sm">{errors.password}</p>
          )}
        </div>
        <p className="text-sm text-center p-4 font-polySans">
          By clicking &quot;Sign in,&quot; you agree to our{" "}
          <a
            href="https://www.udacity.com/legal/terms-of-service"
            className="underline"
          >
            Terms of Use
          </a>{" "}
          and our{" "}
          <a href="https://www.udacity.com/legal/privacy" className="underline">
            Privacy Policy
          </a>
          .
        </p>
        <button
          type="submit"
          onClick={handleSubmit}
          className={clsx(
            "w-full bg-blue-700 hover:bg-blue-600 text-white font-bold py-2 px-4 rounded focus:outline-none focus:bg-blue-600",
            isLoading && "opacity-50 cursor-not-allowed"
          )}
          disabled={isLoading}
        >
          Sign In
        </button>
        <div>
          <a
            href="#"
            className="block py-4 text-blue-900 font-medium mb-2 text-center"
          >
            Forgot your password?
          </a>

          <div className="h-5 border-b border-gray-400 p-2 text-center">
            <label className="bg-white px-5 font-medium">or</label>
          </div>

          <a
            href="#"
            className="block py-6 text-blue-900 font-medium mb-2 text-center"
          >
            Sign in with your organization.
          </a>
        </div>
      </div>
    </div>
  );
};

export default SignInForm;
