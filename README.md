# Udacity Clone

This project is a clone of the Udacity login/signup page, aiming to replicate the design and functionality of the [original page](https://auth.udacity.com/sign-up). It is built using Next.js.

## Features
- Replicates the design and functionality of the original page.
- Provides a signin and signup feature at page (`localhost:3000`).
- Built with Next.js for a server-rendered React application.

## Getting started
1. **Clone the Repository**: Clone this repository with specific branch `code-review` using Git by running the following command in your terminal:
    ```
     git clone -b code-review https://gitlab.com/goutam.verma/udacity-clone.git
    ```
2. **Navigate to the Project Directory**: Move into the cloned directory using:
    ```
    cd udacity-clone
    ```
3. **Install Dependencies**: Install the required dependencies using npm or yarn:
    ```
    npm install 
    ```
    or
    ```
    yarn install
    ```
4.  **Run in Development Mode**: To run the project in development mode, use:
    ```
    npm run dev
    ```
    This will start the development server on `localhost:3000`.

5. **Build for Production**: If you want to build the project for production, you can use:
    ```
    npm run build
    ```
6. **Run in Production Mode**: After building, you can start the production server with:
    ```
    npm start
    ```

## Usage
Once the development server is running or the production server is started, you can access the Udacity login/signup page clone in your browser by navigating to localhost:3000.

- To access the signin and signup page, go to `localhost:3000`.